--Written for RoR by Sullemunk
ding = {}
Iconshow = {}
level = GameData.Player.level


function ding.LevelUp()
--career icons
Iconshow[20] = "<icon=20189>" --	Ironbreaker x
Iconshow[21] = "<icon=20188>"--		Slayer x
Iconshow[22] = "<icon=20193>"--		Runepriest x
Iconshow[23] = "<icon=20187>"--		Engineer x
Iconshow[24] = "<icon=20182>"--		Blackorc x
Iconshow[25] = "<icon=20184>"--		Choppa x
Iconshow[26] = "<icon=20195>"--		Shammy x
Iconshow[27] = "<icon=20197>"--		Squig x
Iconshow[60] = "<icon=20202>"--		Witchhunter x
Iconshow[61] = "<icon=20190>"--		Kotbs x
Iconshow[62] = "<icon=20183>"--		Brightwizzard x
Iconshow[63] = "<icon=20199>"--		WarriorPriest x
Iconshow[64] = "<icon=20185>"--		Chosen x
Iconshow[65] = "<icon=20192>"--		Marauder x
Iconshow[66] = "<icon=20203>"--		Zealot x
Iconshow[67] = "<icon=20191>"--		Magus x
Iconshow[100] = "<icon=20198>"--	SwordMaster x
Iconshow[101] = "<icon=20194>"--	Shadowwarrior x
Iconshow[102] = "<icon=20200>"--	WhiteLion x
Iconshow[103] = "<icon=20180>"--	ArcMage x
Iconshow[104] = "<icon=20181>"--	BlackGuard x
Iconshow[105] = "<icon=20201>"--	WitchElf x
Iconshow[106] = "<icon=20186>"--	DoK x
Iconshow[107] = "<icon=20196>"--	Sorc x



--sends the message to chat :
SendChatText(towstring("/shout !Ding   <br>"..Iconshow[GameData.Player.career.id])..L"<LINK data=\"PLAYER:"..GameData.Player.name..L"\" text=\""..GameData.Player.name..L"\" color=\"0,255,0\"> ("..GameData.Player.Renown.curTitle..L") <icon=52><LINK data=\"0\" text=\"LvL:"..GameData.Player.level..L"\" color=\"125,255,125\"> <icon=45><LINK data=\"0\" text=\"RR:"..GameData.Player.Renown.curRank..L"\" color=\"225,125,225\">", L"")

end

function ding.Initialize()
LibSlash.RegisterSlashCmd("ding",ding.LevelUp)

	TextLogAddEntry("Chat", 0, L"<icon=57> Ding v1.2.1 Loaded")


--RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED,"ding.Level" )


RegisterEventHandler( SystemData.Events.ENTER_WORLD, "ding.unload" )
RegisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "ding.unload" )	
RegisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED, "ding.unload")
RegisterEventHandler(SystemData.Events.LOADING_BEGIN, "ding.unload")
RegisterEventHandler(SystemData.Events.LOADING_END, "ding.unload")
end

function ding.unload()
UnregisterEventHandler(SystemData.Events.PLAYER_CAREER_RANK_UPDATED, "ding.LevelUp")
UnregisterEventHandler(SystemData.Events.PLAYER_RENOWN_RANK_UPDATED, "ding.LevelUp")
RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED,"ding.Level" )
end

function ding.Level()

RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RANK_UPDATED, "ding.LevelUp")
RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_RANK_UPDATED, "ding.LevelUp")
UnregisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED,"ding.Level" )
end




